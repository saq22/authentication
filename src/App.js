import React, { Component } from 'react';
import { View
        , Text 
    } from 'react-native';
import firebase from 'firebase';
import { Header
        , Button
        , Spinner
        , CardSection
    } from './Components/common';
import LoginForm from './Components/LoginForm'

class App extends Component {

    state = { loggedIn: null }

    componentWillMount() {
        firebase.initializeApp({
            apiKey: 'AIzaSyCh4E7c_6fOdKo4Ne3IlGXRumJXAx3vjvc',
            authDomain: 'authentication-c73fd.firebaseapp.com',
            databaseURL: 'https://authentication-c73fd.firebaseio.com',
            projectId: 'authentication-c73fd',
            storageBucket: 'authentication-c73fd.appspot.com',
            messagingSenderId: '415540992937'
        });

        firebase.auth().onAuthStateChanged((user) => {
            // onAuthStateChanged covers 
            if(user) {
                this.setState({ loggedIn: true });
            } else {
                this.setState({ loggedIn: false });
            }
        });
    }

    renderContent() {

        switch(this.state.loggedIn) {

            case true: 
                return(
                    <CardSection>
                            <Button onPress={() => firebase.auth().signOut()}>
                                Log out
                            </Button>
                    </CardSection>
                );

            case false:
                return <LoginForm />;

            default:
                return <View style={{ height: 100 }}><Spinner style={{ marginTop: 30 }} size="large" /></View>;
        }
    }

    render() {
        return(
            <View>
                <Header headerText="Authentication" />
                {this.renderContent()}
            </View>
        )
    }
}

export default App;