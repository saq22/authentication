import React, { Component } from 'react';
import { Text } from 'react-native';
import firebase from 'firebase';
import { 
        Button
        , Card
        , CardSection
        , Input
        , Spinner
    } from './common';

class LoginForm extends Component {
    // constructor() {
    //     super()
    //     this.state = {
    //         email: '',
    //         password: '',
    //     };
    // }

    state ={
        email: ''
        , password: ''
        , error: ''
        , loading: false
    };
    onButtonPress() {
        const { email, password } = this.state;

        firebase.auth().signInWithEmailAndPassword(email, password)
        .then(this.onLoginSuccess.bind(this))
        .catch(() => {
            firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(this.onLoginSuccess.bind(this))
            .catch(this.onLoginFailed.bind(this));
        });

        this.setState({
            error: ''
            , loading: true
        });
    };

    onLoginFailed() {
        this.setState({ 
            error: 'Authentication failed!'
            , loading: false
        });
    }

    onLoginSuccess() {
        this.setState({ 
            email: ''
            , password: ''
            , loading: false
            , error: ''
        });
    }

    renderButton() {
        if (this.state.loading) {
            return(
                <Spinner 
                    size="small" />
            )
        }
        return(
            <Button 
                onPress={this.onButtonPress.bind(this)}
                >Log in
            </Button>
        )
    }

    render() {
        return(
            <Card>
                <CardSection>
                    <Input 
                        label="Email"
                        placeholder="user@gmail.com"
                        onChangeText={email => this.setState({ email })} 
                        value={this.state.email} 
                    />
                </CardSection>
                <CardSection>
                    <Input 
                        label="Password"
                        placeholder="password"
                        onChangeText={password => this.setState({ password })} 
                        value={this.state.password}
                        secureTextEntry />
                </CardSection>

                <Text 
                    style={styles.errorTextStyle}
                    >{this.state.error}</Text>

                <CardSection>
                    {this.renderButton()}
                </CardSection>
            </Card>
        )
    }
}
const styles ={
    errorTextStyle: {
        fontSize: 20,
        alignSelf: 'center',
        color: '#f00',
    }
}
export default LoginForm;