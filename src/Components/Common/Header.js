

// Import libraries for making a components
import React from 'react';
import { Text, View } from 'react-native';

// Make a components
const Header = (props) => {
    const { textStyle, viewStyle } = styles;

    return (
        <View style={viewStyle}>
            <Text style={textStyle}>{props.headerText}</Text>
        </View>
    );
};

const styles = {
    viewStyle: {
        backgroundColor: '#007aff',
        justifyContent: 'center',
        alignItems: 'center',
        height: 70,
        shadowColor: '#000',
        shadowOffset: { width: 3, height: 15 },
        shadowOpacity: 1,
        position: 'relative',
        // paddingTop: 5,
    },
    textStyle: {
        fontSize: 26,
    }
};

// Make the component available to other parts of the app
export { Header };

