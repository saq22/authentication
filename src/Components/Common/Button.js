import React from 'react';
import { 
    Text
    , TouchableOpacity 
    } from 'react-native';

const Button = ({ onPress, children }) => {
    const { buttonStyle, textStyle } = styles;
    return (
        <TouchableOpacity
        style={buttonStyle} 
        onPress={onPress}
        ><Text 
            style={textStyle}
            >{children}</Text>
        </TouchableOpacity>
    );
};

const styles = {
    buttonStyle: {
        flex: 1,
        alignItems: 'stretch',
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#007aff',
        borderRadius: 10,
        marginHorizontal: 5,
    },
    textStyle: {
        alignSelf: 'center',
        color: '#007aff',
        fontSize: 16,
        fontWeight: '600',
        paddingVertical: 10,
    },
}

// Make the component available to other parts of the app. Notice we didn't use export default.
// { Button: Button } => { Button }
export { Button };